import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class CountAllGtcWurcs extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs_count_all_gtc_wurcs" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <template is="dom-repeat" items="{{sampleids}}">
      <p>{{item.numbers}}</p>
    </template>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('count-all-gtc-wurcs', CountAllGtcWurcs);
