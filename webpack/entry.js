import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@webcomponents/webcomponentsjs/webcomponents-lite.js';
import '../repo-detectformat.js';
import '../count-gfc-wurcs.js';
import '../count-gfc-error.js';
import '../count-all-gtc-wurcs.js';
